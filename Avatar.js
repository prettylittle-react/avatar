// https://codyhouse.co/ds/components/app/avatar

import React from 'react';
import PropTypes from 'prop-types';

// import Img from 'img';
import {Content, Editable, Heading, Img, RichText, Text} from 'editable';

import {getModifiers} from 'libs/component';

import './Avatar.scss';

/**
 * Avatar
 * @description [description]
 * @example
  <div id="Avatar"></div>
  <script>
	ReactDOM.render(React.createElement(Components.Avatar, {
	}), document.getElementById("Avatar"));
  </script>
 */
const Avatar = props => {
	const baseClass = 'avatar';

	const {name, image, size, initials} = props;

	const atts = {
		title: name || undefined,
		className: getModifiers(baseClass, [size])
	};

	if (name || (initials && !image)) {
		const label =
			initials ||
			name
				.split(' ')
				.map(part => {
					return part
						.trim()
						.charAt(0)
						.toUpperCase();
				})
				.join('');

		return <span {...atts} data-initials={label} />;
	}

	return (
		<span {...atts}>
			<Img {...image} />
		</span>
	);
};

Avatar.defaultProps = {
	size: 'default',
	initials: '',
	name: '',
	image: null
};

Avatar.propTypes = {
	size: PropTypes.string,
	initials: PropTypes.string,
	name: PropTypes.string,
	image: PropTypes.object
};

export default Avatar;
